# coding: utf-8
from math import sqrt  # Importation racine carée
import tkinter as tk  # Importation de tkinter pour pouvoir créer une interface graphique
#from PIL import ImageTk, Image  # Importation de PIL pour gérer les images
import tkinter.messagebox  # Importation de messagebox pour les pop-ups.

window = tk.Tk()  # Création d'une fenêtre avec tkinter
window.title('Uniformly accelerated motion calculator')  # Nom de la fenêtre
window.geometry('800x300')  # Dimensions de la fenêtre
window.resizable(height = None)
window.configure(bg="#18191c")

#arrow = Image.open("arrow.png")  # Importation de l'image de la flèche
#arrow = ImageTk.PhotoImage(arrow)  # Création de la variable contenant la flèche
#ARROW = tk.Label(window, image=arrow)  # Création de la variable tkinter contenant la flèche
#ARROW.configure(bg="#18191c")

# Création des cinq variables nécessaires à un MRUA
V0 = None  # Vitesse initiale
V1 = None  # Vitesse finale
A = None  # Accélération
D = None  # Distance
T = None  # Temps

# Création des Entry pour les variables
inputV0 = tk.Entry(window, show=None, font=('Arial', 14), width=12)
inputV1 = tk.Entry(window, show=None, font=('Arial', 14), width=12)
inputA = tk.Entry(window, show=None, font=('Arial', 14), width=12)
inputD = tk.Entry(window, show=None, font=('Arial', 14), width=12)
inputT = tk.Entry(window, show=None, font=('Arial', 14), width=12)

# Apparition des Entry
inputV0.grid(row=0, column=1, sticky="we", pady=(10, 0), padx=(0, 5))
inputV1.grid(row=1, column=1, sticky="we", padx=(0, 5))
inputA.grid(row=2, column=1, sticky="we", padx=(0, 5))
inputD.grid(row=3, column=1, sticky="we", padx=(0, 5))
inputT.grid(row=4, column=1, sticky="we", padx=(0, 5))

# Création des Labels pour les Entry
lblV0 = tk.Label(window, text="Initial Speed", bd=3, bg="#36393f", fg="#b9bbbe")
lblV1 = tk.Label(window, text="Final Speed", bd=3, bg="#36393f", fg="#b9bbbe")
lblA = tk.Label(window, text="Acceleration", bd=3, bg="#36393f", fg="#b9bbbe")
lblD = tk.Label(window, text="Distance", bd=3, bg="#36393f", fg="#b9bbbe")
lblT = tk.Label(window, text="Time", bd=3, bg="#36393f", fg="#b9bbbe")

# Apparition des Labels
lblV0.grid(row=0, column=0, sticky="we", padx=(20, 5), pady=(10, 0))
lblV1.grid(row=1, column=0, sticky="we", padx=(20, 5))
lblA.grid(row=2, column=0, sticky="we", padx=(20, 5))
lblD.grid(row=3, column=0, sticky="we", padx=(20, 5))
lblT.grid(row=4, column=0, sticky="we", padx=(20, 5))

# Création des Labels des résultats
police = ('Arial', 17)
otpV0 = tk.Label(window, bd=3, bg="#18191c", fg="#b9bbbe", font=police)
otpV1 = tk.Label(window, bd=3, bg="#18191c", fg="#b9bbbe", font=police)
otpA = tk.Label(window, bd=3, bg="#18191c", fg="#b9bbbe", font=police)
otpD = tk.Label(window, bd=3, bg="#18191c", fg="#b9bbbe", font=police)
otpT = tk.Label(window, bd=3, bg="#18191c", fg="#b9bbbe", font=police)

# Création des Labels contenant les unités des résultats
otpV0unit = tk.Label(window, bd=3, bg="#18191c", fg="#b9bbbe", font=police)
otpV1unit = tk.Label(window, bd=3, bg="#18191c", fg="#b9bbbe", font=police)
otpAunit = tk.Label(window, bd=3, bg="#18191c", fg="#b9bbbe", font=police)
otpDunit = tk.Label(window, bd=3, bg="#18191c", fg="#b9bbbe", font=police)
otpTunit = tk.Label(window, bd=3, bg="#18191c", fg="#b9bbbe", font=police)

otpLabelV0 = tk.Label(window, bd=3, text='V0 = ', bg="#18191c", fg="#b9bbbe", font=police)
otpLabelV1 = tk.Label(window, bd=3, text='V1 = ', bg="#18191c", fg="#b9bbbe", font=police)
otpLabelA = tk.Label(window, bd=3, text='A = ', bg="#18191c", fg="#b9bbbe", font=police)
otpLabelD = tk.Label(window, bd=3, text='D = ', bg="#18191c", fg="#b9bbbe", font=police)
otpLabelT = tk.Label(window, bd=3, text='T = ', bg="#18191c", fg="#b9bbbe", font=police)

bgclearBtn = "#a2112f"
abgclearBtn = "#fc647c"
hlclearbutton = "#f6052d"
clearBtnV0 = tk.Button(window, bd=3, text='Clear', bg=bgclearBtn, activebackground=abgclearBtn, width=3, highlightcolor=hlclearbutton)
clearBtnV1 = tk.Button(window, bd=3, text='Clear', bg=bgclearBtn, activebackground=abgclearBtn, width=3, highlightcolor=hlclearbutton)
clearBtnA = tk.Button(window, bd=3, text='Clear', bg=bgclearBtn, activebackground=abgclearBtn, width=3, highlightcolor=hlclearbutton)
clearBtnD = tk.Button(window, bd=3, text='Clear', bg=bgclearBtn, activebackground=abgclearBtn, width=3, highlightcolor=hlclearbutton)
clearBtnT = tk.Button(window, bd=3, text='Clear', bg=bgclearBtn, activebackground=abgclearBtn, width=3, highlightcolor=hlclearbutton)


class Clearmanagement:
	def __init__(self, inputX):
		self.inputX = inputX

	def clearing(self):
		self.inputX.delete(0, 'end')


rowV0 = Clearmanagement(inputV0)
rowV1 = Clearmanagement(inputV1)
rowA = Clearmanagement(inputA)
rowD = Clearmanagement(inputD)
rowT = Clearmanagement(inputT)

clearBtnV0.config(command=rowV0.clearing, highlightbackground="#18191c")
clearBtnV0.grid(row=0, column=4, sticky="we", padx=(10, 5), pady=(10, 0))
clearBtnV1.config(command=rowV1.clearing, highlightbackground="#18191c")
clearBtnV1.grid(row=1, column=4, sticky="we", padx=(10, 5))
clearBtnA.config(command=rowA.clearing, highlightbackground="#18191c")
clearBtnA.grid(row=2, column=4, sticky="we", padx=(10, 5))
clearBtnD.config(command=rowD.clearing, highlightbackground="#18191c")
clearBtnD.grid(row=3, column=4, sticky="we", padx=(10, 5))
clearBtnT.config(command=rowT.clearing, highlightbackground="#18191c")
clearBtnT.grid(row=4, column=4, sticky="we", padx=(10, 5))

Ltofind = []  # Contient les variables à trouver en string
Lresult = []  # Contient les tout les résultats


def presubmit():
	global V0, V1, A, D, T
	global UnitV0, UnitV1, UnitA, UnitD, UnitT, Ltofind, Numberofvariables, decimales
	NoError = True
	Numberofvariables = 0
	Ltofind = []

	# Récupératio du contenu des Entry
	V0 = inputV0.get()
	V1 = inputV1.get()
	A = inputA.get()
	if A == "g":
		A = 9.80665
	D = inputD.get()
	T = inputT.get()

	# Récupération des unités sélectionnées
	UnitV0 = tkvarV0.get()
	UnitV1 = tkvarV1.get()
	UnitA = tkvarA.get()
	UnitD = tkvarD.get()
	UnitT = tkvarT.get()

	# Définition du nombre de décimales maximum à cacluler
	decimales = int(tkvarDec.get())

	try:  # Le try sert ici à vérifier que les Entry ne contiennent que des chiffres

		# Vérification caractères spéciaux
		if V0 != '':  # On vérifie uniquement quand l'Entry n'est pas vide
			V0 = float(
				V0)  # Si python ne pavient pas à transformer la variable en float, c'est que V0 est un string, et donc qu'il contient des caractères non-authorisés
			Numberofvariables += 1
		elif V0 == '':
			V0 = None
			if "V0" not in Ltofind:
				Ltofind.append('V0')  # On met la variable dans la liste de variables à trouver

		if V1 != '':
			V1 = float(V1)
			Numberofvariables += 1
		elif V1 == '':
			V1 = None
			if "V1" not in Ltofind:
				Ltofind.append('V1')

		if A != '':
			A = float(A)
			Numberofvariables += 1
		elif A == '':
			A = None
			if "A" not in Ltofind:
				Ltofind.append('A')

		if D != '':
			D = float(D)
			Numberofvariables += 1
		elif D == '':
			D = None
			if "D" not in Ltofind:
				Ltofind.append('D')

		if T != '':
			T = float(T)
			Numberofvariables += 1
		elif T == '':
			T = None
			if "T" not in Ltofind:
				Ltofind.append('T')
	except:
		tk.messagebox.showinfo('Erreur',
							   "Veuillez vérifiez que les lacunes ne contiennent que des chiffres")  # Message d'erreur
		NoError = False

	if "A" not in Ltofind and "V0" not in Ltofind and "V1" not in Ltofind:
		if A < 0 and V1 > V0:
			tk.messagebox.showinfo('Erreur',
								   "La VITESSE FINALE (V1) ne peut pas être plus grande que la VITESSE INITIALE (V0) dans le cas d'une décélération.")
			NoError = False

		if A > 0 and V0 > V1:
			tk.messagebox.showinfo('Erreur',
								   "La VITESSE INITIALE (V0) ne peut pas être plus grande que la VITESSE FINALE (V1) dans le cas d'une accélération")
			NoError = False

	if "T" not in Ltofind:
		if T < 0:
			tk.messagebox.showinfo('Erreur', "Le TEMPS ne peut pas être négatif, en tout cas pour l'instant")
			NoError = False

	if Numberofvariables < 3:
		tk.messagebox.showinfo('Erreur', "Veuillez indiquer au moins trois variables")
		NoError = False

	if NoError == True:
		print("VARIABLES A CALCULER: ", Ltofind)
		print("PRESUBMIT: AUCUNE ERREUR A SIGNALER")
		submit()


def submit():  # Fonction s'exécutant lors de l'activation du bouton SUBMIT
	# On rend ici les variables contenant les unités globale, pour pouvoir les modifier de manière globale
	global UnitV0, UnitV1, UnitA, UnitD, UnitT
	global V0, V1, A, D, T

	# Disparition des résultats précédents
	otpV0.grid_forget()
	otpV1.grid_forget()
	otpA.grid_forget()
	otpD.grid_forget()
	otpT.grid_forget()

	#ARROW.grid_forget()  # Disparion de la flèche

	# Disparition des unités des résultats précédents
	otpV0unit.grid_forget()
	otpV1unit.grid_forget()
	otpAunit.grid_forget()
	otpDunit.grid_forget()
	otpTunit.grid_forget()

	# Disparition des Labels des résultats précédents
	otpLabelV0.grid_forget()
	otpLabelV1.grid_forget()
	otpLabelA.grid_forget()
	otpLabelD.grid_forget()
	otpLabelT.grid_forget()

	# Transformation des unités dans le système international
	if UnitV0 == "cm/s" and V0 != None:
		V0 = V0 / 100
	elif UnitV0 == "km/s" and V0 != None:
		V0 = V0 * 1000
	elif UnitV0 == "km/h" and V0 != None:
		V0 = V0 / 3.6

	if UnitV1 == "cm/s" and V1 != None:
		V1 = V1 / 100
	elif UnitV1 == "km/s" and V1 != None:
		V1 = V1 * 1000
	elif UnitV1 == "km/h" and V1 != None:
		V1 = V1 / 3.6

	if UnitD == "mm" and D != None:
		D = D / 1000
	elif UnitD == "cm" and D != None:
		D = D / 100
	elif UnitD == "km" and D != None:
		D = D * 1000

	if UnitT == "ms" and T != None:
		T = T / 1000

	print("TRANSFORMATION UNITES: CHECK")

	resolveurEquation(V0, V1, A, D, T)  # Résolution du système d'équation


def resolveurEquation(V0, V1, A, D, T):  # Fonction responsable de résoudre le MRUA
	global Lresult, UnitV0, UnitV1, UnitA, UnitD, UnitT, Numberofvariables

	NoError2 = True

	V0old = V0
	V1old = V1
	Aold = A
	Dold = D
	Told = T

	if V0 is None:
		if V1 is None:
			V0 = (2 * D - A * T ** 2) / 2 * T
			V1 = V0 + A * T
		if A is None:
			A = 2 * (V1 * T - D) / T ** 2
			V0 = V1 - A * T
		if D is None:
			V0 = V1 - A * T
			D = 0.5 * A * T ** 2 + V0 * T
		if T is None:
			V0 = sqrt(V1 ** 2 - 2 * A * D)
			T = 2 * D / (V0 + V1)
		else:
			V0 = V1 - A * T

	if V1 is None:
		if A is None:
			V1 = 2 * D * T - V0
			A = (V1 - V0) / T
		if D is None:
			V1 = V0 + A * T
			D = 0.5 * A * T ** 2 + V0 * T
		if T is None:
			V1 = sqrt(V0 ** 2 + 2 * A * D)
			T = 2 * D / (V0 + V1)
		else:
			V1 = V0 + A * T

	if A is None:
		if D is None:
			A = (V1 - V0) / T
			D = 0.5 * A * T ** 2 + V0 * T
		if T is None:
			T = 2 * D / (V0 + V1)
			A = (V1 - V0) / T
		else:
			A = (V1 - V0) / T
	if D is None:
		if T is None:
			T = (V1 - V0) / A
			D = 0.5 * A * T ** 2 + V0 * T
		else:
			D = 0.5 * A * T ** 2 + V0 * T
	if T is None:
		T = (V1 - V0) / A

	if Numberofvariables == 4:
		mistake = False
		V0_1 = V1 - A * T
		V0_2 = (2 * D - A * T ** 2) / 2 * T
		if V0_1 != V0_2:
			mistake = True

		V1_1 = V0 + A * T
		V1_2 = 2 * D * T - V0
		if V1_1 != V1_2:
			mistake = True

		A_1 = 2 * (V1 * T - D) / T ** 2
		A_2 = (V1 - V0) / T
		if A_1 != A_2:
			mistake = True

		D_1 = 0.5 * A * T ** 2 + V0 * T
		D_2 = 0.5 * (V0 + V1) * T
		if D_1 != D_2:
			mistake = True

		T_1 = 2 * D / (V0 + V1)
		T_2 = (V1 - V0) / A
		if T_1 != T_2:
			mistake = True

		if mistake == True:
			NoError2 = False
			error()
		else:
			print("PAS D'INCOHERENCE: CHECK")
			
	if Numberofvariables == 5:
		stupide = tk.messagebox.askyesno("Application","Ètes-vous stupide?")
		while stupide == False:
			stupide = tk.messagebox.askyesno("Application","Ètes-vous stupide?")
		NoError2 = False
	
	if NoError2 == True:
		# Reconversion dans les unités sélectionnées par l'utilisateur
		if UnitV0 == "cm/s":
			V0 = V0 * 100
		elif UnitV0 == "km/s":
			V0 = V0 / 1000
		elif UnitV0 == "km/h":
			V0 = V0 * 3.6

		if UnitV1 == "cm/s":
			V1 = V1 * 100
		elif UnitV1 == "km/s":
			V1 = V1 / 1000
		elif UnitV1 == "km/h":
			V1 = V1 * 3.6

		if UnitD == "mm":
			D = D * 1000
		elif UnitD == "cm":
			D = D * 100
		elif UnitD == "km":
			D = D / 1000

		if UnitT == "ms":
			T = T * 1000

		print("RECONVERSION DES UNITES: CHECK")
		print("RESOLUTION: CHECK")

		Lresult = [V0, V1, A, D, T]  # Les résultats sont stocké dans Lresult
		#ARROW.grid(row=0, column=5, rowspan=5, padx=40, pady=(10,0))  # Apparition de la flèche
		OutputResult()  # Rendu des résultats


def error():
	tk.messagebox.showinfo('Erreur', "Not scientifically possible")


def OutputResult():  # Fonction qui s'occupe de faire apparaître les résultats
	global Ltofind, Lresult
	print(Lresult)
	V0 = round(Lresult[0], decimales)  # On arrondis les résultats au nombre de décimales souhaitées (5 par défaut)
	if V0 - int(V0) == 0:
		V0 = int(V0)  # Si on a un nombre entier, on le retransforme en int pour plus de lisibilité.

	V1 = round(Lresult[1], decimales)
	if V1 - int(V1) == 0:
		V1 = int(V1)

	A = round(Lresult[2], decimales)
	if A - int(A) == 0:
		A = int(A)

	D = round(Lresult[3], decimales)
	if D - int(D) == 0:
		D = int(D)

	T = abs(round(Lresult[4], decimales))
	if T - int(T) == 0:
		T = int(T)

	print("ARRONDISSEMENT DES RESULTATS: CHECK")
	print("Vi = ", Lresult[0], "\nVf = ", Lresult[1], "\na = ", Lresult[2], "\nd = ", Lresult[3], "\nt = ", Lresult[4])

	# Apparition résultat V0
	otpLabelV0.grid(row=0, column=6, pady=(10, 0), sticky="e")
	otpV0.config(text=V0)
	otpV0.grid(row=0, column=7, pady=(10, 0))
	otpV0unit.config(text=UnitV0)
	otpV0unit.grid(row=0, column=8, sticky="w", pady=(10, 0))

	# Apparition résultat V1
	otpLabelV1.grid(row=1, column=6, sticky="e")
	otpV1.config(text=V1)
	otpV1.grid(row=1, column=7)
	otpV1unit.config(text=UnitV1)
	otpV1unit.grid(row=1, column=8, sticky="w")

	# Apparition résultat A
	otpLabelA.grid(row=2, column=6, sticky="e")
	otpA.config(text=A)
	otpA.grid(row=2, column=7)
	otpAunit.config(text=UnitA)
	otpAunit.grid(row=2, column=8, sticky="w")

	# Apparition résultat D
	otpLabelD.grid(row=3, column=6, sticky="e")
	otpD.config(text=D)
	otpD.grid(row=3, column=7)
	otpDunit.config(text=UnitD)
	otpDunit.grid(row=3, column=8, sticky="w")

	# Apparition résultat T
	otpLabelT.grid(row=4, column=6, sticky="e")
	otpT.config(text=T)
	otpT.grid(row=4, column=7)
	otpTunit.config(text=UnitT)
	otpTunit.grid(row=4, column=8, sticky="w")


def reset():
	global V0, V1, A, D, T, Ltofind

	Ltofind = []  # Vidage de Ltofind

	# Remise à zéro des variables
	V0 = None
	V1 = None
	A = None
	D = None
	T = None

	# Vidage des Entry
	inputV0.delete(0, 'end')
	inputV1.delete(0, 'end')
	inputA.delete(0, 'end')
	inputD.delete(0, 'end')
	inputT.delete(0, 'end')

	# Disparition des résultats
	otpV0.grid_forget()
	otpV1.grid_forget()
	otpA.grid_forget()
	otpD.grid_forget()
	otpT.grid_forget()

	# On remet les unités de base
	tkvarV0.set(UnitsV0[1])
	tkvarV1.set(UnitsV1[1])
	tkvarA.set(UnitsA[0])
	tkvarD.set(UnitsD[2])
	tkvarT.set(UnitsT[1])

	#ARROW.grid_forget()  # Disparion de la flèche

	# Disparition des unités des résultats
	otpV0unit.grid_forget()
	otpV1unit.grid_forget()
	otpAunit.grid_forget()
	otpDunit.grid_forget()
	otpTunit.grid_forget()

	# Disparition des Labels des résultats
	otpLabelV0.grid_forget()
	otpLabelV1.grid_forget()
	otpLabelA.grid_forget()
	otpLabelD.grid_forget()
	otpLabelT.grid_forget()

# Création des menus dropdown pour les unités
ddactivebg = "#36393f"

UnitsV0 = ["cm/s", "m/s", "km/s", "km/h"]  # On met toutes les possibilités dans une liste
tkvarV0 = tk.StringVar(window)  # On crée la variable qui contient le choix sélectionné
tkvarV0.set(UnitsV0[1])  # On choisit la valeur d'unité par défaut
UnitV0Menu = tk.OptionMenu(window, tkvarV0, *UnitsV0)  # On crée le menu dropdown
UnitV0Menu.configure(highlightbackground="#18191c", bg="#36393f", fg="#b9bbbe", activebackground="#303238")
UnitV0Menu["menu"].config(bg=ddactivebg, activebackground="#303238", fg="#b9bbbe")
UnitV0Menu.grid(row=0, column=2, sticky="we", pady=(10, 0))

UnitsV1 = ["cm/s", "m/s", "km/s", "km/h"]
tkvarV1 = tk.StringVar(window)
tkvarV1.set(UnitsV1[1])
UnitV1Menu = tk.OptionMenu(window, tkvarV1, *UnitsV1)
UnitV1Menu.configure(highlightbackground="#18191c", bg="#36393f", fg="#b9bbbe", activebackground="#303238")
UnitV1Menu["menu"].config(bg=ddactivebg, activebackground="#303238", fg="#b9bbbe")
UnitV1Menu.grid(row=1, column=2, sticky="we")

UnitsA = ["m/s2"]
tkvarA = tk.StringVar(window)
tkvarA.set(UnitsA[0])
UnitAMenu = tk.OptionMenu(window, tkvarA, *UnitsA)
UnitAMenu.configure(highlightbackground="#18191c", bg="#36393f", fg="#b9bbbe", activebackground="#303238")
UnitAMenu["menu"].config(bg=ddactivebg, activebackground="#303238", fg="#b9bbbe")
UnitAMenu.grid(row=2, column=2, sticky="we")

UnitsD = ["mm", "cm", "m", "km"]
tkvarD = tk.StringVar(window)
tkvarD.set(UnitsD[2])
UnitDMenu = tk.OptionMenu(window, tkvarD, *UnitsD)
UnitDMenu.configure(highlightbackground="#18191c", bg="#36393f", fg="#b9bbbe", activebackground="#303238")
UnitDMenu["menu"].config(bg=ddactivebg, activebackground="#303238")
UnitDMenu.grid(row=3, column=2, sticky="we")

UnitsT = ["ms", "s"]
tkvarT = tk.StringVar(window)
tkvarT.set(UnitsT[1])
UnitTMenu = tk.OptionMenu(window, tkvarT, *UnitsT)
UnitTMenu.configure(highlightbackground="#18191c", bg="#36393f", fg="#b9bbbe", activebackground="#303238")
UnitTMenu["menu"].config(bg=ddactivebg, activebackground="#303238", fg="#b9bbbe")
UnitTMenu.grid(row=4, column=2, sticky="we")

def changedecimales(a):
	global decimales
	decimales = int(tkvarDec.get())
	print(decimales)
	OutputResult()

decimalespossibles = [2,3,4,5,6,7,8,9]
tkvarDec = tk.StringVar(window)
tkvarDec.set(decimalespossibles[0])

UnitDecMenu = tk.OptionMenu(window, tkvarDec, *decimalespossibles, command=changedecimales)
UnitDecMenu.configure(highlightbackground="#18191c", width=4)
UnitDecMenu.grid(row=6, column=5, padx=40)

lblDec = tk.Label(window, text="Decimals")
lblDec.config(bd=3, bg="#36393f", fg="#b9bbbe")
lblDec.grid(row=5, column=5, padx=40, pady=(5,5))

bouton = tk.Button(text="SUBMIT", command=presubmit, bg="#36393f", fg="#b9bbbe", highlightbackground="#18191c")  # Création du bouton SUBMIT
bouton.grid(column=1, row=5, sticky='we', padx=(0,5))  # Apparition du bouton SUBMIT
bouton1 = tk.Button(text="RESET", command=reset, bg="#36393f", fg="#b9bbbe", highlightbackground="#18191c")  # Création du bouton SUBMIT
bouton1.grid(column=1, row=6, sticky='we', padx=(0,5))   # Apparition du bouton RESET

window.mainloop()  # Apparition de la fenêtre
